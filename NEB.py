#!/usr/bin/env python
import numpy as np
import copy


class NudgedElasticBand(object):
    # initializes an NEB calculation
    def __init__(self, energy_fn, grad_fn,
                 dt=0.01, max_iters=1000, damping=1.0,
                 dist_tol=1e-4, energy_tol=1e-4,
                 climbing_force=False, convergence_window=5):
        self.energy_fn = energy_fn
        self.grad_fn = grad_fn
        self.dt = dt
        self.max_iters = max_iters
        self.damping = damping
        self.dist_tol = dist_tol
        self.energy_tol = energy_tol
        self.climbing = climbing_force
        self.convergence_window = convergence_window
        if self.climbing:
            self.force = self._climbing_force
        else:
            self.force = self._force

    @staticmethod
    def _local_tangent_simple(x):
        tangent = np.zeros(x.shape)
        n = x.shape[0]
        # don't calculate tangent for end points
        for i in range(1, n-1):
            disp = x[i+1] - x[i-1]
            tangent[i] = disp/np.linalg.norm(disp)
        return tangent

    @staticmethod
    def _local_tangent_bisect(x):
        tangent = np.zeros(x.shape)
        n = x.shape[0]
        # don't calculate tangent for end points
        for i in range(1, n-1):
            left = x[i] - x[i-1]
            right = x[i+1] - x[i]
            left = left/np.linalg.norm(left)
            right = right/np.linalg.norm(right)
            tangent[i] = left + right
            tangent[i] = tangent[i]/np.linalg.norm(tangent[i])
        return tangent

    def _local_tangent_best(self, x, v, debug=True):
        # for this method, need also the energies
        tangent = np.zeros(x.shape)
        n = x.shape[0]
        branch = np.zeros(n)
        # don't calculate tangent for end points
        for i in range(1, n-1):
            # work it out here
            ti_plus = x[i+1] - x[i]
            ti_minus = x[i] - x[i-1]
            if v[i+1] > v[i] > v[i-1]:
                tangent[i] = ti_plus
                branch[i] = 1
            elif v[i+1] < v[i] < v[i-1]:
                tangent[i] = ti_minus
                branch[i] = 2
            elif ((v[i+1] > v[i] < v[i-1]) or
                    (v[i+1] < v[i] > v[i-1])):
                # take weighted average of two vectors
                vi_plus = v[i+1] - v[i]
                vi_minus = v[i-1] - v[i]
                dvi_max = max(abs(vi_minus), abs(vi_plus))
                dvi_min = min(abs(vi_minus), abs(vi_plus))
                if v[i+1] > v[i-1]:
                    tangent[i] = (ti_plus * dvi_max +
                                  ti_minus * dvi_min)
                    branch[i] = 3
                elif v[i+1] < v[i-1]:
                    tangent[i] = (ti_plus * dvi_min +
                                  ti_minus * dvi_max)
                    branch[i] = 4
                # not covered by improved tangent method
                # two energies are the same:
                # fall back to bisection
                else:
                    tangent[i] = self._local_tangent_bisect(x[i-1:i+2])[1]
                    branch[i] = 5
            else:
                # Probably shouldn't be here - that energy surface
                # is flat, and all energies are the same:
                # Not covered by improved tangent method,
                # instead use the simple version (which is more robust
                # than the bisection method)
                # (pass points at either side - take middle one back)
                tangent[i] = self._local_tangent_simple(x[i-1:i+2])[1]
                branch[i] = 6
            tangent[i] = tangent[i]/np.linalg.norm(tangent[i])
        if debug:
            if np.sum(np.isnan(tangent)):
                for i, t in enumerate(tangent):
                    if np.sum(np.isnan(t)):
                        print('NaNtan: x', x[i-1:i+2], 'Tangent:', t,
                              'V:', v[i-1:i+2], 'Branch:', branch[i])
        return tangent

    @staticmethod
    def _spring_force_simple(elastic_band, tangent, energies):
        spring_force = np.zeros(elastic_band.positions.shape)
        # don't calculate spring force on end points
        # account for varying spring consts (energies ignored if not needed)
        spring_const = elastic_band.spring_const(energies)
        # print('spring_const', spring_const)
        x = elastic_band.positions
        for i in range(1, elastic_band.num_images-1):
            disp = (spring_const[i+1]*(x[i+1] - x[i]) -
                    spring_const[i]*(x[i] - x[i-1]))
            proj = np.dot(disp, tangent[i])
            spring_force[i] = proj*tangent[i]
        return spring_force

    @staticmethod
    def _spring_force_improved(elastic_band, tangent, energies):
        # unused at the moment
        spring_force = np.zeros(elastic_band.positions.shape)
        # don't calculate spring force on end points
        # account for varying spring consts (energies ignored if not needed)
        spring_const = elastic_band.spring_const(energies)
        x = elastic_band.positions
        for i in range(1, elastic_band.num_images-1):
            disp = (np.linalg.norm(spring_const[i+1]*(x[i+1] - x[i])) -
                    np.linalg.norm(spring_const[i]*(x[i] - x[i-1])))
            spring_force[i] = disp*tangent[i]
        return spring_force

    @staticmethod
    def _true_perp_force(tangent, gradient):
        proj = np.zeros(gradient.shape)
        for i in range(proj.shape[0]):
            proj[i] = np.dot(gradient[i], tangent[i]) * tangent[i]
        return -(gradient - proj)

    def V(self, elastic_band):
        return self.energy_fn(elastic_band.positions)

    def _neb_total_force(self, elastic_band, tangent, gradient, energies):
        spring_force = self._spring_force_simple(elastic_band,
                                                 tangent, energies)
        true_perp_force = self._true_perp_force(tangent, gradient)
        return spring_force + true_perp_force

    def _force(self, elastic_band):
        energies = self.V(elastic_band)
        gradient = self.grad_fn(elastic_band.positions)
        tangent = self._local_tangent_best(elastic_band.positions, energies)
        return self._neb_total_force(elastic_band, tangent, gradient, energies)

    def _climbing_force(self, elastic_band):
        energies = self.V(elastic_band)
        gradient = self.grad_fn(elastic_band.positions)
        tangent = self._local_tangent_best(elastic_band.positions, energies)
        force = self._neb_total_force(elastic_band, tangent,
                                      gradient, energies)
        # image with highest energy (not including end points)
        max_image = np.argmax(energies[1:-1]) + 1
        true_force_max_image = -self.grad_fn(elastic_band.positions[max_image])
        tangent_max_image = tangent[max_image]
        force[max_image] = (true_force_max_image -
                            2*tangent_max_image*(
                                np.dot(true_force_max_image,
                                       tangent_max_image))
                            )
        return force

    def relax(self, elastic_band, debug=False):
        initial_band = copy.deepcopy(elastic_band)
        tmp_band = copy.deepcopy(elastic_band)
        final_band = self._neb_optimize_velocity_verlet(tmp_band, debug)
        self.min_max_energy_images(initial_band)
        self.min_max_energy_images(final_band)
        return initial_band, final_band

    def min_max_energy_images(self, elastic_band):
        energies = self.V(elastic_band)
        min_image = np.argmin(energies[1:-1]) + 1
        max_image = np.argmax(energies[1:-1]) + 1
        elastic_band.min_energy_image = min_image
        elastic_band.max_energy_image = max_image
        elastic_band.min_energy = energies[min_image]
        elastic_band.max_energy = energies[max_image]

    def _calc_additive_energy(self, elastic_band):
        return np.sum(self.V(elastic_band))

    @staticmethod
    def _calc_update_magnitude(prev_state, new_state):
        return np.apply_along_axis(
            np.linalg.norm,
            arr=new_state - prev_state,
            axis=1
        )

    def _vv_convergence(self, prev_pos, new_pos,
                        prev_energy, new_energy,
                        forces, debug=False):

        energy_diff = np.abs(prev_energy - new_energy)
        max_dist = np.max(self._calc_update_magnitude(prev_pos, new_pos))
        energy_ok = energy_diff <= self.energy_tol
        dist_ok = max_dist <= self.dist_tol
        if debug:
            print('Energy diff:', energy_diff)
        return energy_ok and dist_ok

    def _neb_optimize_velocity_verlet(self, elastic_band, debug=False):
        # set up positions, velocities and forces
        pos = elastic_band.positions[1:elastic_band.num_images-1, :]
        vel = np.zeros(pos.shape)

        # need all positions to get forces
        force = self.force(elastic_band)
        # only need forces acting on unfixed images
        force = force[1:elastic_band.num_images-1, :]
        energy = self._calc_additive_energy(elastic_band)
        elastic_band.relax_energies = []
        elastic_band.relax_energies.append(energy)

        # how many consecutive times have we been within tolerance?
        conv_window = 0
        lowest_energy = energy
        lowest_energy_band = copy.deepcopy(elastic_band)

        for i_iter in range(self.max_iters):
            # store previous positions and energy
            # to measure convergence
            prev_pos = pos.copy()
            prev_energy = energy

            # update to half timestep velocity
            acc = force/elastic_band.mass
            vel = vel + acc*self.dt/2

            # update positions using half timestep velocity
            # if we apply additional damping, can do so here
            # (where 0 < damping <= 1)
            vel = vel*self.damping

            # pos_to_move = pos_to_move + damping*vel*dt
            pos = pos + vel*self.dt
            elastic_band.positions[1:elastic_band.num_images-1] = pos

            # get new force - need all band positions
            force = self.force(elastic_band)
            force = force[1:elastic_band.num_images-1, :]
            acc = force/elastic_band.mass

            # we want only the component of the velocity
            # that is parallel to the force
            for i in range(vel.shape[0]):
                force_dir = force[i]/np.linalg.norm(force[i])
                vel[i] = np.dot(vel[i], force_dir) * force_dir

            # finally update the velocities
            # to next timestep
            vel = vel + self.dt*acc/2

            # check for convergence
            energy = self._calc_additive_energy(elastic_band)
            elastic_band.relax_energies.append(energy)
            if energy < lowest_energy:
                lowest_energy = energy
                lowest_energy_band = copy.deepcopy(elastic_band)

            if self._vv_convergence(prev_pos, pos,
                                    prev_energy, energy,
                                    force, debug):
                conv_window += 1
                if conv_window == self.convergence_window:
                    if energy > lowest_energy:
                        print("NEB: Energy increased from lowest "
                              "energy band — returning this instead")
                    return lowest_energy_band
            else:
                conv_window = 0
        print ('NEB: Did not converge to relaxed state. '
               'Returning lowest energy band')
        return lowest_energy_band


class ElasticBandCreator(object):
    def __init__(self, start_point, end_point, num_images,
                 spring_const=100.0, mass=1.0,
                 delta_spring_const=0.0,
                 ref_energy=None,
                 variable_springs=False,
                 noisy=True, noise=0.1):

        self.start_point = start_point
        self.end_point = end_point
        self.num_images = num_images
        self.mass = mass
        self.delta_spring_const = delta_spring_const
        self.ref_energy = ref_energy
        if variable_springs:
            self.max_spring_const = spring_const
            self.spring_const = self._variable_spring_consts
        else:
            # always access via a function
            # match call signature of variable spring consts:
            def _spring_const(energies=None):
                return np.ones(self.num_images)*spring_const
            self.spring_const = _spring_const

        if noisy:
            self._noisy_elastic_band(noise)
        else:
            self._elastic_band()

    def _elastic_band(self):
        ndims = self.start_point.size
        positions = np.zeros((self.num_images, ndims))
        for i in range(ndims):
            col = np.linspace(self.start_point[i], self.end_point[i],
                              self.num_images)
            positions[:, i] = col
        self.positions = positions
        return self.positions

    def _noisy_elastic_band(self, noise=0.1):
        # needs improving, but will do for now...
        positions = self._elastic_band()
        ndims = positions.shape[1]
        for i in range(1, self.num_images-1):
            tmp_pos = np.zeros(ndims)
            for j, comp in enumerate(positions[i]):
                tmp_pos[j] = self._gauss_rand(mean=comp, stddev=noise)
            positions[i] = tmp_pos
        self.positions = positions
        return self.positions

    @staticmethod
    def _gauss_rand(mean=0.0, stddev=1.0):
        return stddev * np.random.randn() + mean

    def _variable_spring_consts(self, energies):
        # pass in energies rather than calc...
        if self.ref_energy is None:
            # just use the max of the end points:
            self.ref_energy = np.max([energies[0], energies[-1]])

        e_max = np.max(energies)
        spring_constants = np.ones(self.num_images) * self.max_spring_const

        # don't actually need to worry about spring constants at the
        # end points, because they're fixed
        for i in range(self.num_images):
            e_i = np.max([energies[i-1], energies[i]])
            if e_i > self.ref_energy:
                scale = (e_max - e_i)/(e_max - self.ref_energy)
            else:
                scale = 1.0
            spring_constants[i] = (self.max_spring_const -
                                   scale*self.delta_spring_const)
        return spring_constants
